\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage{xcolor}
\usepackage{csquotes}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    linkcolor={black!50!black},
    citecolor={black!50!black},
    urlcolor={blue!80!black}
}

\newcounter{question}
\setcounter{question}{0}
\begin{document}

\newcommand\Question[1]{%
   \leavevmode\par
   \stepcounter{question}
   \noindent
   \thequestion. #1\par}

\newcommand\Answer[2][]{%
    \leavevmode\par\noindent
   {\leftskip37pt
    \textbf{#1}#2\par}}

\begin{titlepage}
\vspace*{\stretch{1.0}}
   \begin{center}
   \LARGE
       \textbf{PermafrostNet ERDDAP Server Operation Guide}

       \vspace{0.5cm}

       \textbf{Alex Gao}\\
       \textbf{alexgao@cmail.carleton.ca}
  
   \end{center}
\vspace*{\stretch{1.0}}
\end{titlepage}

\tableofcontents
\newpage

\section{Introduction}
This document concerns the operation of the ERDDAP server used to host permafrost data files. The following links may be of interest:\\
\begin{itemize}
	\item \href{https://coastwatch.pfeg.noaa.gov/erddap/download/setup.html}{Official ERDDAP Setup Instructions}
	\item \href{https://coastwatch.pfeg.noaa.gov/erddap/download/setupDatasetsXml.html}{Official ERDDAP datasets.xml Information}
	\item\href{https://groups.google.com/forum/#!forum/erddap}{Official ERDDAP Google Group for questions}
	\item \href{https://github.com/IrishMarineInstitute/awesome-erddap}{Github repository of interesting ERDDAP related links}
	\item Append \textit{/status.html} to the end of your basic ERDDAP server URL to view a status page similar to that seen in the logs (.../erddap/status.html)
\end{itemize}

\section{Turning the ERDDAP Server On and Off}
To turn this off, you should be the \textbf{tomcat} user. (To be the tomcat user, use \textit{sudo su - tomcat}.) Navigate to /usr/local/apache-tomecat-x.x.xx/bin.
\begin{itemize}
	\item To start the server, run \textit{bash startup.sh} . This will take a while, depending on the number of files loaded in. It can range from 10 - 30 minutes.
	\item To stop the server, run \textit{bash shutdown.sh} . Then, verify with \textit{ps -ef $\vert$ grep tomcat} and ensure that there isn't an active Java instance running from the results. This may take a few minutes. \textbf{Ensure that the server has fully stopped (read: no longer appears in the ps -ef results) before starting it up again.}
\end{itemize}

\section{Notable Areas}
\textbf{/usr/local/apache-tomcat-x.x.xx/bin}
\begin{displayquote}
\textit{Contains important scripts for the server. Mainly used ones are the startup and shutdown scripts.}\\
\end{displayquote}

\noindent\textbf{/usr/local/apache-tomcat-x.x.xx/content/erddap}
\begin{displayquote}
\textit{Location of the datasets.xml file that is read by ERDDAP.}\\
\end{displayquote}

\noindent\textbf{/usr/local/apache-tomcat-x.x.xx/webapps/erddap/WEB-INF}
\begin{displayquote}
\textit{Contains some utility scripts, most notably, GenerateDatasetsXml.}\\
\end{displayquote}

\noindent\textbf{/home/tomcat/erddap/logs}
\begin{displayquote}
\textit{Where log files from server operation and GenerateDatasetsXml are stored. log.txt contains the most recent logs and is recommended to be checked for any sign of problems from the server.}\\
\end{displayquote}

\section{Questions}
\Question{How does one properly remove a dataset?}
\Answer{Edit the datasets.xml file and set the ``active'' attribute to false. Then, on the next time where ERDDAP has a major loadDatasets event, it will remove it from the list of accessible datasets. At that point, the entry can be removed from datasets.xml and the files deleted from the server.}

\Question{Something in the aggregate dataset is not loading even after waiting a few hours. Why not?}
\Answer{The aggregate dataset may require a restart (or you may need to wait longer than the regular interval of the \textit{major load datasets} event). It is likely that an error occurred within the aggregate dataset, but it is not reported immediately. It is more likely to be reported after restarting. You could try waiting a day or two if you want to avoid restarting.}

\Question{An error of ``ERROR: Invalid Table: Duplicate column names: [...] and [...] are both ...'' is reported in the logs, what does it mean?}
\Answer{You cannot have duplicate hard-coded values (this is likely to come up in the metadata columns of datasets.xml). This is about the aggregate dataset where you are providing metadata attributes. Oftentimes this is due to the same URL given for a dataset's \textit{infoUrl} and \textit{publisher\_url} properties. These need to be different.}

\section{Original Setup Instructions}
\begin{verbatim}
https://coastwatch.pfeg.noaa.gov/erddap/download/setup.html#initialSetup

1. Set up Java 8.
	Go to https://adoptopenjdk.net/releases.html
	Choose version OpenJDK 8 (LTS), JVM HotSpot
	(Select appropriate OS, in this case, I selected Linux x64)
	Download the prebuilt binary aka .tar.gz file
	Extract the contents to	(Original setup recommends /usr/local) to /usr/local
	Check that it was installed correctly, example:
		user@taiga:~$ /usr/local/jdk8u252-b09/jre/bin/java -version
		openjdk version "1.8.0_252"
		OpenJDK Runtime Environment (AdoptOpenJDK)(build 1.8.0_252-b09)
		OpenJDK 64-Bit Server VM (AdoptOpenJDK)(build 25.252-b09, mixed mode)

2. Set up Tomcat
	Go to https://tomcat.apache.org/download-80.cgi
		Download the "Core" .tar.gz
		Extract, it is recommended to extract to /usr/local
		
	Go to /usr/local/apache-tomcat-x.x.xx/conf
		Edit the server.xml file with the changes indicated 
			"""
			server.xml - In tomcat/conf/server.xml file, there are two changes that you should make
			 to each of the two <Connector> tags
			 (one for '<Connector port="8080"' and one for '<Connector port="8443"'):
			Increase the connectionTimeout parameter value, 
			perhaps to 300000 (milliseconds) (which is 5 minutes).
				Add a new parameter: relaxedQueryChars="[]|"
			"""
			In this case, the server.xml only had an uncommented out <Connector> tag for port 8080.
			<Connector port="8080" protocol="HTTP/1.1"
               connectionTimeout="300000"
               redirectPort="8443"
               relaxedQueryChars="[]|" />
			For the <Connector> of port 8443:
				Omitted
				
		Edit the context.xml with the changes indicated:
			"""
			In your [tomcat]/conf/context.xml, add this tag right before </Context> :
			<Resources cachingAllowed="true" cacheMaxSize="80000" />
			"""
			
		Edit the Apache httpd.conf file:
			Omitted - does not exist at this point
			
		Edit apache-tomcat-x.x.xx/conf/web.xml
			Add the following within <web-app>:
				<security-constraint>
					<web-resource-collection>
						<web-resource-name>restricted methods</web-resource-name>
						<url-pattern>/*</url-pattern>
						<http-method>TRACE</http-method>
						<http-method>PUT</http-method>
						<http-method>OPTIONS</http-method>
						<http-method>DELETE</http-method>
					</web-resource-collection>
					<auth-constraint />
				</security-constraint>
		
	Setting up Tomcat as user "tomcat" as limited permission user
		(Follow page instructions:
			You can create the tomcat user account (which has no password) by using the command
			sudo useradd tomcat -s /bin/bash -p '*'
			You can switch to working as user tomcat by using the command
			sudo su - tomcat
			(It will ask you for the superuser password for permission to do this.)
			You can stop working as user tomcat by using the command
			exit
			Do most of the rest of the Tomcat and ERDDAP setup instructions as user "tomcat".
			Later, run the startup.sh and shutdown.sh scripts as user "tomcat" so that
			Tomcat has permission to write to its log files.
			After unpacking Tomcat, from the parent of the apache-tomcat directory:
			Change ownership of the apache-tomcat directory tree to the tomcat user.
			chown -R tomcat apache-tomcat-8.5.51
			(but substitute the actual name of your tomcat directory).
			Change the "group" to be tomcat, your username, or the name of a small group that
			includes tomcat and all the administrators of Tomcat/ERDDAP, e.g.,
			chgrp -R yourUserName apache-tomcat-8.5.51
			Change permissions so that tomcat and the group have read, write, execute privileges, e.g,.
			chmod -R ug+rwx apache-tomcat-8.5.51
			Remove "other" user's permissions to read, write, or execute:
			chmod -R o-rwx apache-tomcat-8.5.51
			This is important, because it prevents other users from reading 
			possibly sensitive information in ERDDAP setup files.
		)
		
	Setting up Tomcat environment variables
		Create [tomcat installation]/bin/setenv.sh file
			Contains (but change the directory names to match):
				export JAVA_HOME=/usr/local/jdk8u252-b09/jre
				export JAVA_OPTS='-server -Djava.awt.headless=true -Xmx3800M -Xms3800M'
				export TOMCAT_HOME=/usr/local/apache-tomcat-8.5.54
				export CATALINA_HOME=/usr/local/apache-tomcat-8.5.54
			The 1500M values (referring to the page's example) can be changed depending on the system memory.
			In this case, there is about 8GB of memory, so we will give
			just under half, 3800M instead for both instances of 1500M. (-Xmx3800M -Xms3800M)
			-d64 is added because we are on a 64 bit OS and we downloaded 64 bit Java
			
	On Linux and Macs, change the permissions of all *.sh files in tomcat/bin/ to be executable
	by the owner, e.g., with
		chmod +x *.sh
		(Note: Is this fine? "other" has execute access, the setenv.sh also has other read access)
		tomcat@taiga:/usr/local/apache-tomcat-8.5.54/bin$ ls -la
		total 876
		drwxrwx---  2 tomcat tomcat   4096 May  5 15:46 .
		drwxrwx--- 10 tomcat tomcat   4096 May  5 18:17 ..
		-rwxrwx---  1 tomcat tomcat  35204 Apr  3 14:07 bootstrap.jar
		-rwxrwx---  1 tomcat tomcat   1664 Apr  3 14:09 catalina-tasks.xml
		-rwxrwx---  1 tomcat tomcat  16608 Apr  3 14:07 catalina.bat
		-rwxrwx--x  1 tomcat tomcat  24397 Apr  3 14:07 catalina.sh
		-rwxrwx---  1 tomcat tomcat   2123 Apr  3 14:07 ciphers.bat
		-rwxrwx--x  1 tomcat tomcat   1997 Apr  3 14:07 ciphers.sh
		-rwxrwx---  1 tomcat tomcat 206895 Apr  3 14:07 commons-daemon-native.tar.gz
		-rwxrwx---  1 tomcat tomcat  25197 Apr  3 14:07 commons-daemon.jar
		-rwxrwx---  1 tomcat tomcat   2040 Apr  3 14:07 configtest.bat
		-rwxrwx--x  1 tomcat tomcat   1922 Apr  3 14:07 configtest.sh
		-rwxrwx--x  1 tomcat tomcat   9127 Apr  3 14:07 daemon.sh
		-rwxrwx---  1 tomcat tomcat   2091 Apr  3 14:07 digest.bat
		-rwxrwx--x  1 tomcat tomcat   1965 Apr  3 14:07 digest.sh
		-rwxrwx---  1 tomcat tomcat   3460 Apr  3 14:07 setclasspath.bat
		-rwxrwx--x  1 tomcat tomcat   3708 Apr  3 14:07 setclasspath.sh
		-rwxrwxr-x  1 tomcat tomcat    226 May  5 15:46 setenv.sh
		-rwxrwx---  1 tomcat tomcat   2020 Apr  3 14:07 shutdown.bat
		-rwxrwx--x  1 tomcat tomcat   1902 Apr  3 14:07 shutdown.sh
		-rwxrwx---  1 tomcat tomcat   2022 Apr  3 14:07 startup.bat
		-rwxrwx--x  1 tomcat tomcat   1904 Apr  3 14:07 startup.sh
		-rwxrwx---  1 tomcat tomcat  51261 Apr  3 14:07 tomcat-juli.jar
		-rwxrwx---  1 tomcat tomcat 419428 Apr  3 14:07 tomcat-native.tar.gz
		-rwxrwx---  1 tomcat tomcat   4574 Apr  3 14:07 tool-wrapper.bat
		-rwxrwx--x  1 tomcat tomcat   5540 Apr  3 14:07 tool-wrapper.sh
		-rwxrwx---  1 tomcat tomcat   2026 Apr  3 14:07 version.bat
		-rwxrwx--x  1 tomcat tomcat   1908 Apr  3 14:07 version.sh

	(Font installation, at this point the setup.xml doesn't exist yet)
	
	Test Tomcat installation
		
		As user "tomcat", run tomcat/bin/startup.sh
		
		If Tomcat is already running, shut down Tomcat with (in Linux or Mac OS) tomcat/bin/shutdown.sh
		On Linux, use ps -ef | grep tomcat before and after shutdown.sh to make sure
		the tomcat process has stopped.
		The process should be listed before the shutdown and eventually not listed after the shutdown.
		It may take a minute or two for ERDDAP to fully shut down. Be patient.
		Or if it looks like it won't stop on its own, use:
		kill -9 processID
		
3. Set up tomcat/content/erddap configuration files
	(Installed unzip)
	Extract the erddapContent.zip file, move the content folder to [tomcat installation]
	Edit the setup.xml in that content folder [tomcat]/content/erddap/setup.xml
		bigParentDirectory is set to /home/tomcat/erddap/
			Go to /home/, as root, create /tomcat directory, 
			then go in to /tomcat, and create /erddap directory. 
			While in the /home/tomcat directory, make tomcat the owner of erddap
				root@taiga:/home/tomcat# chown -R tomcat erddap/
			Make tomcat group as well
				root@taiga:/home/tomcat# chgrp -R tomcat erddap/
			Give tomcat user and tomcat group rwx permissions on the directory
				root@taiga:/home/tomcat# chmod -R ug+rwx erddap/
			Remove other permission for rwx
				root@taiga:/home/tomcat# chmod -R o-rwx erddap/
			Result:
				root@taiga:/home/tomcat# ls -la
				total 12
				drwxr-xr-x 3 root   root   4096 May  5 18:19 .
				drwxr-xr-x 5 root   root   4096 May  5 18:19 ..
				drwxrwx--- 2 tomcat tomcat 4096 May  5 18:19 erddap
		baseUrl is set to http://206.12.93.125:8080
		Change all the required fields (including fieldKeyKey otherwise errors will be thrown)

	datasets.xml file is recommended to be modified at this point, but leaving it as default for now.
	
	Make a copy of "erddapStart2.css" and rename the copy to "erddap2.css" for
	future CSS changes to ERDDAP. Changes to this erddap2.css file only take place when
	the system is restarted and end users clear their caches.
	
	Navigate to [tomcat]/webapps
		Download https://github.com/BobSimons/erddap/releases/download/v2.02/erddap.war
		
	Installed apache2
	Went to /etc/apache2/apache2.conf
		Change Timeout to 3600
		
	Go to etc/apache2/sites-enabled/000-default.conf
		Add into the VirtualHost tag
			ServerName 206.12.93.125
			ProxyRequests Off
			ProxyPreserveHost On
			ProxyPass /erddap http://localhost:8080/erddap
			ProxyPassReverse /erddap http://localhost:8080/erddap
			
	Add following to apache2.conf as before. Added after the IncludeOptional mods-enabled
		LoadModule proxy_module /usr/lib/apache2/modules/mod_proxy.so
		LoadModule proxy_http_module /usr/lib/apache2/modules/mod_proxy_http.so
		LoadModule proxy_wstunnel_module /usr/lib/apache2/modules/mod_proxy_wstunnel.so
		LoadModule rewrite_module /usr/lib/apache2/modules/mod_rewrite.so	
			
	Reset with sudo /usr/sbin/apachectl -k graceful
	
	Can visit 
	http://206.12.93.125:8080
	http://206.12.93.125 
	now
	and go to http://206.12.93.125/erddap instead
	
	Created in /usr/local/jdk8u252-b09/lib/
		a file called fontconfig.properties
		that contains
		version=1
		sequence.allfonts=default
		(not sure if this is necessary)
	
	Installed: sudo apt install fontconfig
\end{verbatim}

\end{document}
