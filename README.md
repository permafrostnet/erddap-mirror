# Running as a service
See [this Gist](https://gist.github.com/drmalex07/e6e99dad070a78d5dab24ff3ae032ed1) for an example of how to configure a systemd service to run  (hint the `type=forking` parameter is important)

## Other Permafrost ERDDAP Data
* USGS Climate Monitoring
   - [Ikpikpuk](http://erddap.aoos.org/erddap/tabledap/gov_usgs_climate_monitoring_8.html)
   - [Full List](http://erddap.aoos.org/erddap/tabledap/gov_usgs_climate_monitoring_9153574.html)

## Other Canadian ERDDAP Data
   - [Water survey]()
