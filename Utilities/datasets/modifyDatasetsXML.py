from lxml import etree as xmlTree

def xmlToString(block):
    return "".join([xmlTree.tostring(tag, encoding="unicode", pretty_print=True) for tag in block])

def insertXMLBlock(xmlBlocks, targetFile, aggregateID, outputFile):
    '''Inserts xml blocks into an ERDDAP datasets.xml
    Inserts blocks as child element under specified tags
    
    :xmlBlock: LIST - list of dictionaries of strings of blocks of XML to be inserted and boolean indicating if it is an aggregate dataset
        Dictionaries have `data` for XML string and `isAggregate` boolean
    :targetFile: FILE POINTER - file pointer to the datasets.xml file opened
    :aggregateName: STRING - string of dataset ID for the aggregate dataset
    :outputFile: STRING - string of path where the resulting XML document should be written to
    '''
    parser = xmlTree.XMLParser(strip_cdata=False)
    xmlDoc = xmlTree.parse(targetFile, parser=parser)
    erddapDatasets = xmlDoc.getroot()
    
    # Prevent the generation of self closing tags, set text to empty string to prevent this
    for el in erddapDatasets.iter():
        if el.text == None:
            el.text = ''

    aggregateNode = erddapDatasets.xpath(f"/erddapDatasets/dataset[@datasetID='{aggregateID}']")
    if len(aggregateNode) < 1:
        raise Exception(f"No dataset with ID:{aggregateID} was found!")
    else:
        aggregateNode = aggregateNode[0]

    for newBlock in xmlBlocks:
        if newBlock['isAggregate']:
            aggregateNode.append(xmlTree.fromstring(newBlock['data']))
        else:
            erddapDatasets.append(xmlTree.fromstring(newBlock['data']))
    
    xmlDoc.write(outputFile, xml_declaration=True, pretty_print=True, encoding='ISO-8859-1', method='XML')
    
def editXMLBlock(xmlBlocks, targetFile, outputFile):
    '''Updates existing dataset blocks of datasets.xml
    Finds the existing tag(s) within the file and updates it with the given XML strings
    
    :xmlBlock: LIST - list of dictionaries of strings and datasetIDs of xml blocks to replace the existing blocks
        Dictionaries have properties `data` containing the XML string and `id` containing the datasetID string to find within the datasets.xml file by
    :targetFile: FILE POINTER - file pointer to the datasets.xml file opened
    :outputFile: STRING - string of path where the resulting XML document should be written to
    '''
    parser = xmlTree.XMLParser(strip_cdata=False)
    xmlDoc = xmlTree.parse(targetFile, parser=parser)
    erddapDatasets = xmlDoc.getroot()
    
    # Prevent the generation of self closing tags, set text to empty string to prevent this
    for el in erddapDatasets.iter():
        if el.text == None:
            el.text = ''
    
    for updatedBlock in xmlBlocks:
        block = erddapDatasets.xpath(f"//dataset[@datasetID='{updatedBlock['id']}']")
        if len(block) > 0:
            blockParent = block[0].getparent()
            blockParent.remove(block[0])
            blockParent.append(xmlTree.fromstring(updatedBlock['data']))
        else:
            raise Exception(f"No dataset with ID:{updatedBlock['id']} was found!")

    xmlDoc.write(outputFile, xml_declaration=True, pretty_print=True, encoding='ISO-8859-1', method='XML')
    
def getXMLBlock(datasetID, targetFile):
    '''Gets an entire "dataset" XML block from ERDDAP datasets.xml by datasetID
    
    :datasetID: STRING - The ID of the dataset to be returned
    :targetFile: FILE POINTER - file pointer to the datasets.xml file opened
    '''
    parser = xmlTree.XMLParser(strip_cdata=False)
    xmlDoc = xmlTree.parse(targetFile, parser=parser)
    erddapDatasets = xmlDoc.getroot()
    
    block = erddapDatasets.xpath(f"//dataset[@datasetID='{datasetID}']")
    if len(block) > 0:
        return block
    else:
        raise Exception(f"No dataset with ID:{datasetID} was found!")

def getAttributeValue(xmlBlock, attributeName):
    '''Gets value of an attribute from an xmlBlock
    
    :xmlBlock: XML Tree Element - Element of XML tree
    :attributeName: STRING - Name of attribute whose value is to be returned
    '''
    return xmlBlock[0].xpath(f"./addAttributes//att[@name='{attributeName}']")[0].text

if __name__ == '__main__':
    pass
