# fmtValues = {
    # "datasetID": "pfnetGrndTmpAll",
    # "directory_name": "123",
    # "creator_name": "Bob",
    # "infoUrl": "www.google.ca",
    # "institution": "Carleton",
    # "summary": "...",
    # "title": "research",
    # "publisher_name": "Carleton",
    # "publisher_email": "Carleton",
    # "publisher_url": "Carleton",
    # "project": "my sample project 123"
# }
import json
import modifyDatasetsXML as mdxml

dtSt = ""
aggSt = ""
info = ""

with open("fString_Template.txt", "r") as fs:
    dtSt = fs.read()
    
with open("fString_Template+Metadata.txt", "r") as fs:
    aggSt = fs.read()
    
with open("json file that was generated from the dataset upload form", "r") as dat:
    info = json.loads(dat.read())

with open("source datasets.xml location", "r+") as f:
    mdxml.insertXMLBlock([
    {"data": dtSt.format(**info), "isAggregate": False},
    {"data": aggSt.format(**info), "isAggregate": True}
    ], f, "pfnetGrndTmpAll", "where you want your new dataset.xml file to be saved")